package com.assetjapan.employee_manager_assetjapan.repositories;

import com.assetjapan.employee_manager_assetjapan.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findRoleByName(String name);
}
