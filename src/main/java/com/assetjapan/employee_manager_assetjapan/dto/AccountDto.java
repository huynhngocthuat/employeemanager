package com.assetjapan.employee_manager_assetjapan.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AccountDto {
    private long accountId;

    @NotEmpty
    @Length(min = 6)
    private String username;

    @NotEmpty
    @Length(min = 6)
    private String password;

    @NotEmpty
    private int role;

    @NotEmpty
    private String fullName;

    @NotEmpty
    private String faculty;

    @NotEmpty
    private String address;
}
