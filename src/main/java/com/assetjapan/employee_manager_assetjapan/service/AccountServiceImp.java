package com.assetjapan.employee_manager_assetjapan.service;

import com.assetjapan.employee_manager_assetjapan.model.Account;
import com.assetjapan.employee_manager_assetjapan.model.Role;
import com.assetjapan.employee_manager_assetjapan.repositories.AccountRepository;
import com.assetjapan.employee_manager_assetjapan.repositories.RoleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class AccountServiceImp implements AccountService, UserDetailsService {

    private final AccountRepository accountRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.findAccountByUsername(username);
        if (account == null){
            log.error("Account not found in the database");
            throw new UsernameNotFoundException("Account not found in the database");

        }else {
            log.info("Account found in the database: {}", username);
        }

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        account.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });
        return new org.springframework.security.core.userdetails.User(account.getUsername(), account.getPassword(), authorities);
    }

    @Override
    public Account saveAccount(Account account) {
        log.info("Saving new account {} to the database", account.getFullName());
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        return accountRepository.save(account);
    }

    @Override
    public Role saveRole(Role role) {
        log.info("Saving new role {} to the database", role.getName());
        return roleRepository.save(role);
    }

    @Override
    public void addRoleToAccount(String username, String roleName) {
        log.info("Adding role {} to account {}", roleName, username);
        Account account = accountRepository.findAccountByUsername(username);
        Role role = roleRepository.findRoleByName(roleName);
        ArrayList<Account> list = (ArrayList<Account>) accountRepository.findAll();
        account.getRoles().add(role);
    }

    @Override
    public Account getAccount(String username) {
        log.info("Fetching account {}", username);
        return accountRepository.findAccountByUsername(username);
    }

    @Override
    public List<Account> getAccounts() {
        log.info("Fetching all account");
        return accountRepository.findAll();
    }

    @Override
    public long getIdByUsername(String username) {
        return this.getAccount(username).getAccountId();
    }

    @Override
    public Account getAccountById(long id) {
        return accountRepository.getAccountByAccountId(id);
    }


}
