package com.assetjapan.employee_manager_assetjapan.repositories;

import com.assetjapan.employee_manager_assetjapan.model.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {
    Token findByAccessToken(String accessToken);
    Token findByRefreshToken(String refreshToken);

}
