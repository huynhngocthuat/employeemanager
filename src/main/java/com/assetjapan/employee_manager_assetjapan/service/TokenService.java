package com.assetjapan.employee_manager_assetjapan.service;

import com.assetjapan.employee_manager_assetjapan.model.Token;

import java.util.List;

public interface TokenService {
    Token saveToken(Token token);
    List<Token> findAll();
    Token findByAccessToken(String accessToken);
    Token findByRefreshToken(String refreshToken);
    void deleteToken(Token token);
}
