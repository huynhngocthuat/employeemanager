package com.assetjapan.employee_manager_assetjapan.service;

import com.assetjapan.employee_manager_assetjapan.model.Account;
import com.assetjapan.employee_manager_assetjapan.model.Role;

import java.util.List;

public interface AccountService {
    Account saveAccount(Account account);
    Role saveRole(Role role);
    void addRoleToAccount(String username, String roleName);
    Account getAccount(String username);
    List<Account> getAccounts();
    long getIdByUsername(String username);
    Account getAccountById(long id);
}
