package com.assetjapan.employee_manager_assetjapan.repositories;

import com.assetjapan.employee_manager_assetjapan.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    Account findAccountByUsername(String username);
    Account getAccountByAccountId(long id);
}
