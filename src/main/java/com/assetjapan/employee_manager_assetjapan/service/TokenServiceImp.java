package com.assetjapan.employee_manager_assetjapan.service;

import com.assetjapan.employee_manager_assetjapan.model.Token;
import com.assetjapan.employee_manager_assetjapan.repositories.TokenRepository;
import lombok.RequiredArgsConstructor;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class TokenServiceImp implements TokenService{

    private final TokenRepository tokenRepository;

    @Override
    public Token saveToken(Token token) {
        return this.tokenRepository.saveAndFlush(token);
    }

    @Override
    public List<Token> findAll() {
        return this.tokenRepository.findAll();
    }

    @Override
    public Token findByAccessToken(String accessToken) {
        return this.tokenRepository.findByAccessToken(accessToken);
    }

    @Override
    public Token findByRefreshToken(String refreshToken) {
        return this.tokenRepository.findByRefreshToken(refreshToken);
    }

    @Override
    public void deleteToken(Token token) {
        if (token != null){
            log.info("Delete success {}", token.getTokenId());
            this.tokenRepository.delete(token);
        }else {
            log.info("Delete error token !");
        }
    }


}
