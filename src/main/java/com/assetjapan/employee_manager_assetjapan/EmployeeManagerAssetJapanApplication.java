package com.assetjapan.employee_manager_assetjapan;

import com.assetjapan.employee_manager_assetjapan.model.Account;
import com.assetjapan.employee_manager_assetjapan.model.Role;
import com.assetjapan.employee_manager_assetjapan.service.AccountService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class EmployeeManagerAssetJapanApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeManagerAssetJapanApplication.class, args);
    }

    @Bean
    PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    CommandLineRunner run(AccountService accountService){
        return args -> {
            accountService.saveRole(new Role(1, "ROLE_USER"));

            accountService.saveAccount(new Account(1, "huynhngocthuat", "123456", new ArrayList<>(), "Huynh Ngoc Thuat", "CNTT", "Quang Nam"));
            accountService.saveAccount(new Account(2, "dangcongtoan", "123456", new ArrayList<>(), "Dang Cong Toan", "CNTT", "Da Nang"));
            accountService.saveAccount(new Account(3, "huonglenguyen", "123456", new ArrayList<>(), "Huong Le Nguyen", "CNTT", "Quang Nam"));
            accountService.saveAccount(new Account(4, "doantanty", "123456", new ArrayList<>(), "Doan Tan Ty", "CNTT", "Hue"));

            accountService.addRoleToAccount("huynhngocthuat", "ROLE_USER");
            accountService.addRoleToAccount("dangcongtoan", "ROLE_USER");
            accountService.addRoleToAccount("huonglenguyen", "ROLE_USER");
            accountService.addRoleToAccount("doantanty", "ROLE_USER");
        };
    }
}
