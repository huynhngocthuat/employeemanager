package com.assetjapan.employee_manager_assetjapan.controller;

import com.assetjapan.employee_manager_assetjapan.model.Account;
import com.assetjapan.employee_manager_assetjapan.model.Role;
import com.assetjapan.employee_manager_assetjapan.model.Token;
import com.assetjapan.employee_manager_assetjapan.repositories.TokenRepository;
import com.assetjapan.employee_manager_assetjapan.service.AccountService;
import com.assetjapan.employee_manager_assetjapan.service.TokenService;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AccountController {

    private final AccountService accountService;
    private final TokenService tokenService;

    @GetMapping("/tokens")
    public ResponseEntity<List<Token>> getTokens(){
        return ResponseEntity.ok().body(tokenService.findAll());
    }

    @GetMapping("/accounts")
    public ResponseEntity<List<Account>> getAccounts(){
        return ResponseEntity.ok().body(accountService.getAccounts());
    }

    @GetMapping("/info")
    public Account getAccountById(@RequestAttribute long accountId){
        return accountService.getAccountById(accountId);
    }

    @PostMapping("/account/save")
    public ResponseEntity<Account> saveAccount(@RequestBody Account account){
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/account/save").toUriString());
        return ResponseEntity.created(uri).body(accountService.saveAccount(account));
    }

    @PostMapping("/role/save")
    public ResponseEntity<Role> saveRole(@RequestBody Role role){
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/role/save").toUriString());
        return ResponseEntity.created(uri).body(accountService.saveRole(role));
    }

    @PostMapping("/role/addtoaccount")
    public ResponseEntity<?> addRoleToAccount(@RequestBody RoleToUserForm form){
        accountService.addRoleToAccount(form.getUsername(), form.getRoleName());
        return ResponseEntity.ok().build();
    }

    @PostMapping("/token/refresh")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){
            try {
                String refresh_token = authorizationHeader.substring("Bearer ".length());
                Token verifierToken = tokenService.findByRefreshToken(refresh_token);
                if (verifierToken != null){
                    Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
                    JWTVerifier verifier = JWT.require(algorithm).build();
                    DecodedJWT decodedJWT = verifier.verify(refresh_token);
                    String username = decodedJWT.getSubject();
                    long accountId = decodedJWT.getClaim("accountId").asLong();

                    // refresh access token
                    String refresh_access_token = JWT.create()
                            .withSubject(username)
                            .withExpiresAt(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000))
                            .withIssuer(request.getRequestURL().toString())
                            .withClaim("accountId", accountId)
                            .sign(algorithm);

                    // update access token
                    verifierToken.setAccessToken(refresh_access_token);
                    verifierToken.setAccessTokenExpDate(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000));
                    tokenService.saveToken(verifierToken);

                    Map<String, String> tokens = new HashMap<>();
                    tokens.put("access_token", refresh_access_token);
                    tokens.put("refresh_token", refresh_token);
                    response.setContentType(APPLICATION_JSON_VALUE);
                    new ObjectMapper().writeValue(response.getOutputStream(), tokens);
                }else {
                    response.setContentType(APPLICATION_JSON_VALUE);
                    new ObjectMapper().writeValue(response.getOutputStream(), "Not found refresh token !");
                }

            }
            catch (Exception exception){
                response.setHeader("error", exception.getMessage());
                response.setStatus(FORBIDDEN.value());
                Map<String, String> error = new HashMap<>();
                error.put("error_message", exception.getMessage());
                response.setContentType(APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), error);
            }
        }else {
            throw new RuntimeException("Refresh token is missing");
        }
    }

    @GetMapping("/logout")
    public String logout(){
        return "Logout success !!!";
    }
}

@Data
class RoleToUserForm{
    private String username;
    private String roleName;
}
